# Meteor Angular 2 Router base

This package is build upon Angular2-Meteor-Base example.

## Usage

This project provides a base for angular 2.0 (release) containing basic angular2
and meteor. In order to used it you need to clone it in a new folder and use as
a base.

## Boilerplate Contents

This boilerplate contains the basics that requires to quick start with Angular2-Meteor application.

This package contains:

- TypeScript support and Angular 2 compilers for Meteor
- Angular2-Meteor
- Angular2 (core, common, compiler, platform, router)
- SASS support
- Testing framework with Mocha and Chai
- Material Design (@angular2/material package integration)
- Forms Module is prepared.
- Roboto form with external inclusion
- Collapsed menu (right of the toolbar)


This application also contains demo code:

- Main Component (`/client/app.component`)
- Demo Child Component (`/client/imports/demo/demo.component`)
- Demo Service (`/client/imports/demo/demo-data.service`)
- Demo Mongo Collection (`/both/demo-collection.ts`)
- Routes (`/client/imports/app/app.routing.ts`)
- Home Page (`/client/imports/app/home/home.component.ts`)
- About Page - to show the router is working (`/client/imports/app/about/about.component.ts`)
- Contact Page - Forms demo (`/client/imports/app/contact/contact.component.ts`)
- Basic signup / login / logout functionality (using <login-buttons></login-buttons>)
- Login / Logout / Signup / Recover Components for customization.

#### Common

Example for common files in our app, is the MongoDB collection we create - it located under `/both/demo-collection.ts` and it can be imported from both client and server code.

## TODO
- custom login / logout and signup forms
