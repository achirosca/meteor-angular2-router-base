import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { METEOR_PROVIDERS } from 'angular2-meteor';

import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { DemoDataService } from './demo/demo-data.service';

import { routing,
         appRoutingProviders }  from './app.routing';

import {ReactiveFormsModule} from '@angular/forms';

import {AccountsModule} from 'angular2-meteor-accounts-ui';

import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import { ContactComponent } from './contact/contact.component';

import { LoginComponent} from './auth/login.component';
import { SignupComponent } from './auth/signup.component';
import { RecoverComponent } from './auth/recover.component';


import { MaterialModule } from '@angular/material';

@NgModule({
  // Components, Pipes, Directive
  declarations: [
    AppComponent, DemoComponent,
    HomeComponent, AboutComponent, ContactComponent,
    LoginComponent, SignupComponent, RecoverComponent
  ],
  // Entry Components
  entryComponents: [
    AppComponent
  ],
  // Providers
  providers: [
    appRoutingProviders,
    DemoDataService
  ],
  // Modules
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    MaterialModule.forRoot(),
    AccountsModule,
    routing
  ],
  // Main Component
  bootstrap: [ AppComponent ]
})
export class AppModule {}
