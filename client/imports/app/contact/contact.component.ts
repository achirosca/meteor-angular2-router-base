import {Component} from '@angular/core';
import template from './contact.component.html';

@Component({
  selector:'contact',
  template
})
export class ContactComponent {}
