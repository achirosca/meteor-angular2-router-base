import {Component} from '@angular/core';
import template from './home.component.html';

@Component({
  selector:'home',
  template
})
export class HomeComponent {}
