import { Component, OnInit } from '@angular/core';
import { MeteorComponent } from 'angular2-meteor';
import { Meteor } from 'meteor/meteor';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router} from '@angular/router';

import template from './signup.component.html'

@Component({
  selector:'signup',
  template
})
export class SignupComponent extends MeteorComponent implements OnInit {
  signupForm: FormGroup;
  error: string;

  constructor(
    private router:Router, private formBuilder: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      email: ['',Validators.required],
      password: ['',Validators.required]
    });
    this.error='';
  }

  signup() {
      if (this.signupForm.valid) {
        Accounts.createUser({
          email: this.signupForm.value.email,
          password: this.signupForm.value.password
        }, (err) => {
          if (err) {
            this.error = err;
          } else {
            this.router.navigate(['/']);
          }
        });
      }
    }
}
