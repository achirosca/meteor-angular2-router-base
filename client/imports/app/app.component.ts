import { Component } from '@angular/core';
import { Meteor } from 'meteor/meteor';

import template from './app.component.html';

@Component({
  selector: 'app',
  template
})
export class AppComponent {

  constructor() {
  }

  logout() {
    Meteor.logout();
  }
 }
